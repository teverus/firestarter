import json

import requests


class SquadusAPI:
    BASE_URL = 'https://im.ncloudtech.ru/api/v1'
    HEADERS = {
        'authority': 'im.ncloudtech.ru',
        'accept': '*/*',
        'accept-language': 'en-US,en;q=0.9',
        'content-type': 'application/json',
        'origin': 'https://im.ncloudtech.ru',
        'referer': 'https://im.ncloudtech.ru/group/AQA_Desk_Team?skip_dp=true',
        'x-auth-token': 'null',
        'x-requested-with': 'XMLHttpRequest',
        'x-user-id': 'null',
    }

    def __init__(self, username=None, password=None):
        username = username
        password = password

        self.session = requests.Session()
        self.session.headers.update(self.HEADERS)
        self.auth(username=username, password=password)
        self.url = f'{self.BASE_URL}/method.call'

    def auth(self, username, password):
        data = {
            "msg": "method",
            "id": "4",
            "method": "login",
            "params": [
                {
                    "ldap": True,
                    "username": username,
                    "ldapPass": password,
                    "ldapOptions": {}
                }
            ]
        }
        response = self.session.post(
            f'{self.BASE_URL}/method.callAnon/login',
            data=json.dumps({'message': json.dumps(data)})
        )
        result = json.loads(response.json()['message'])['result']
        self.session.headers['x-auth-token'] = result['token']
        self.session.headers['x-user-id'] = result['id']
        return response.json()

    def get_chat_history(self, chat):
        data = {'msg': 'method',
                'id': '9',
                'method': 'loadHistoryWithFirstUnread',
                'params': [chat, {'$date': 1686654652471}, 500, False]}
        response = self.session.post(f'{self.url}/loadHistoryWithFirstUnread',
                                     data=json.dumps({"message": json.dumps(data)}))
        return list(filter(lambda i: i['msg'] != '', json.loads(response.json()['message'])['result']['messages']))

    def get_inventory_messages(self):
        return self.get_chat_history(chat=Chanel.AQA_Inventory)


class Chanel:
    AQA_Inventory = 'hXhv8oq8hyibEtyPA'
    AQA_Desk_SM_work_notification = 'xobXSRKanGXEQzJxX'
