from dataclasses import dataclass

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMessageBox

from Code.TeverusSDK.Widgets.BaseWidget import Font


class MessageBoxIcon:
    INFORMATION = 1
    QUESTION = 4
    ERROR = 3


class MessageBoxButtonRole:
    YES = QMessageBox.YesRole
    NO = QMessageBox.NoRole


@dataclass
class MessageBoxButton:
    text: str
    role: MessageBoxButtonRole


# noinspection PyTypeChecker
class MessageBox(QMessageBox):
    def __init__(
        self,
        text: str = None,
        window_title: str = " ",
        window_icon: str = "Files/Icons/favicon.png",
        font: Font = Font.REGULAR,
        icon: MessageBoxIcon = None,
        buttons: list[MessageBoxButton] = None,
    ):
        super().__init__()

        # --- Variables ----------------------------------------------------------------
        self.icon = icon
        self.buttons = buttons

        # --- Class characteristics ----------------------------------------------------
        self.setWindowTitle(window_title)
        self.setWindowIcon(QIcon(window_icon))
        self.setText(text)
        self.setFont(font)

        if self.icon:
            self.setIcon(self.icon)

        if self.buttons:
            for button in self.buttons:
                self.addButton(button.text, button.role)

    def show_notification(self) -> int:
        result = self.exec_()
        return result
