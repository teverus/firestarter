from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtWidgets import QWidget


class Font:
    REGULAR = QFont("Candara", 16)
    REGULAR_ITALIC = QFont("Candara", 14, italic=True)
    REGULAR_BOLD = QFont("Candara", 16, QFont.Bold)
    REGULAR_BOLD_BIGGER = QFont("Candara", 20, QFont.Bold)
    TITLE = QFont("Candara", 24, QFont.Bold)


class Alignment:
    CENTER = Qt.AlignCenter
    LEFT = Qt.AlignLeft | Qt.AlignVCenter


class BaseWidget(QWidget):
    def __init__(
        self,
        parent,
        width,
        height,
        right_from=None,
        under=None,
        padding_x=0,
        padding_y=0,
        text=None,
        font=Font.REGULAR,
        icon=None,
        icon_size=None,
        style=None,
        align=None,
        move_to=None,
        clicked=None,
        args=None,
        visible=True,
        disabled=False,
        record_to=None,
        record_as=None,
        show_dimensions=None
    ):
        super().__init__()

        from Code.TeverusSDK.Widgets.ScrollArea import ScrollArea

        self.setParent(parent.inside if isinstance(parent, ScrollArea) else parent)
        self.setFixedSize(width, height)
        self.setVisible(visible)
        self.setDisabled(disabled)

        # --- Positioning --------------------------------------------------------------
        if right_from:
            widget = right_from
            x_delta = widget.pos().x() + widget.width() + padding_x
            y_delta = widget.pos().y() + padding_y
            self.move(x_delta, y_delta)

        if under:
            widget = under
            x_delta = widget.pos().x() + padding_x
            y_delta = under.pos().y() + widget.height() + padding_y
            self.move(x_delta, y_delta)

        if not right_from and not under and padding_x:
            self.move(self.pos().x() + padding_x, self.pos().y())

        if not right_from and not under and padding_y:
            self.move(self.pos().x(), self.pos().y() + padding_y)

        if move_to:
            target_widget = move_to
            x_delta = target_widget.pos().x() + padding_x
            y_delta = target_widget.pos().y() + padding_y
            self.move(x_delta, y_delta)

        # --- Text ---------------------------------------------------------------------
        self.setFont(font)

        if text:
            self.setText(text)

        if align:
            self.setAlignment(align)

        if not text and show_dimensions:
            self.setText(f"{self.width()} x {self.height()}")

        # --- Icon ---------------------------------------------------------------------
        if icon:
            self.setIcon(QIcon(icon))

        if icon_size:
            self.setIconSize(QSize(icon_size, icon_size))

        # --- Style --------------------------------------------------------------------
        if style:
            self.setStyleSheet(style)

        # --- Actions ------------------------------------------------------------------
        if clicked:
            self.clicked.connect(lambda: clicked(*args) if args else clicked())

        # --- Record -------------------------------------------------------------------
        if record_to and record_as:
            record_to.__setattr__(record_as, self)

    def set_visible(self, status):
        self.setVisible(status)
