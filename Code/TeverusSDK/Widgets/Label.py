from PyQt5.QtWidgets import QLabel

from Code.TeverusSDK.Widgets.BaseWidget import BaseWidget, Font, Alignment


class Label(QLabel, BaseWidget):
    def __init__(
        self,
        parent,
        width,
        height,
        padding_x=0,
        padding_y=0,
        right_from=None,
        under=None,
        move_to=None,
        text=None,
        font=Font.REGULAR,
        style=None,
        align=Alignment.CENTER,
        icon=None,
        visible=True,
        disabled=False,
        record_to=None,
        record_as=None,
        show_dimensions=False
    ):
        super(Label, self).__init__(
            parent,
            width,
            height,
            right_from=right_from,
            under=under,
            move_to=move_to,
            padding_x=padding_x,
            padding_y=padding_y,
            text=text,
            font=font,
            style=style,
            align=align,
            icon=icon,
            visible=visible,
            disabled=disabled,
            record_to=record_to,
            record_as=record_as,
            show_dimensions=show_dimensions
        )
