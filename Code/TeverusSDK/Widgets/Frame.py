from PyQt5.QtWidgets import QFrame

from Code.TeverusSDK.Widgets.BaseWidget import BaseWidget, Font


class Frame(QFrame, BaseWidget):
    def __init__(
        self,
        parent,
        width,
        height,
        padding_x=0,
        padding_y=0,
        right_from=None,
        under=None,
        move_to=None,
        text=None,
        font=Font.REGULAR,
        style=None,
        icon=None,
        visible=True,
        disabled=False,
    ):
        super().__init__(
            parent,
            width,
            height,
            right_from=right_from,
            under=under,
            move_to=move_to,
            padding_x=padding_x,
            padding_y=padding_y,
            text=text,
            font=font,
            style=style,
            icon=icon,
            visible=visible,
            disabled=disabled,
        )
