from PyQt5.QtWidgets import QListWidget, QAbstractItemView

from Code.TeverusSDK.Widgets.BaseWidget import BaseWidget, Font


class Listbox(QListWidget, BaseWidget):
    def __init__(
        self,
        parent,
        width,
        height,
        items,
        padding_x=0,
        padding_y=0,
        right_from=None,
        under=None,
        multiple_selection=False,
        item_clicked=None,
        disabled=False,
    ):
        super().__init__(
            parent,
            width,
            height,
            padding_x=padding_x,
            padding_y=padding_y,
            right_from=right_from,
            under=under,
            font=Font.REGULAR,
            style=None,
            disabled=disabled,
        )
        self.addItems(items)

        if multiple_selection:
            self.setSelectionMode(QAbstractItemView.MultiSelection)

        if item_clicked:
            self.itemClicked.connect(item_clicked)
