from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel

from Code.TeverusSDK.Widgets.BaseWidget import BaseWidget


class Image(QLabel, BaseWidget):
    def __init__(
        self,
        parent,
        path_to_image,
        scale_to_width=None,
        scale_to_height=None,
        right_from=None,
        under=None,
        move_to=None,
        padding_x=0,
        padding_y=0,
        style=None,
    ):
        self.pixmap = QPixmap(path_to_image)
        self.scale_to_width = scale_to_width
        self.scale_to_height = scale_to_height

        self.scale_pixmap()

        super().__init__(
            parent=parent,
            width=self.pixmap.width(),
            height=self.pixmap.height(),
            right_from=right_from,
            under=under,
            move_to=move_to,
            padding_x=padding_x,
            padding_y=padding_y,
            style=style,
        )

        self.setPixmap(self.pixmap)

    def set_image(self, image_path: str):
        self.pixmap = QPixmap(image_path)
        self.scale_pixmap()
        self.setPixmap(self.pixmap)

    def scale_pixmap(self):
        if self.scale_to_width:
            self.pixmap = self.pixmap.scaledToWidth(self.scale_to_width, 1)
        elif self.scale_to_height:
            self.pixmap = self.pixmap.scaledToHeight(self.scale_to_height, 1)
