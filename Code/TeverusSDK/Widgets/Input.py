from PyQt5.QtWidgets import QLineEdit

from Code.TeverusSDK.Widgets.BaseWidget import BaseWidget, Font


class Input(QLineEdit, BaseWidget):
    def __init__(
        self,
        parent,
        width,
        height,
        padding_x=0,
        padding_y=0,
        right_from=None,
        under=None,
        disabled=False,
        text=None,
        style=None,
        placeholder=None,
        text_changed=None,
        record_to=None,
        record_as=None,
    ):
        super().__init__(
            parent,
            width,
            height,
            padding_x=padding_x,
            padding_y=padding_y,
            right_from=right_from,
            under=under,
            font=Font.REGULAR,
            disabled=disabled,
            text=text,
            style=style,
            record_to=record_to,
            record_as=record_as,
        )

        if placeholder:
            self.setPlaceholderText(placeholder)

        if text_changed:
            self.textChanged.connect(lambda: text_changed())
