from PyQt5.QtWidgets import QScrollArea, QWidget

from Code.TeverusSDK.Widgets.BaseWidget import BaseWidget


class ScrollArea(QScrollArea, BaseWidget):
    def __init__(
        self,
        parent,
        width,
        height,
        inner_width,
        inner_height,
        padding_x=0,
        padding_y=0,
        right_from=None,
        under=None,
        move_to=None,
        style=None,
        visible=True,
        disabled=False,
    ):
        super().__init__(
            parent,
            width,
            height,
            right_from=right_from,
            under=under,
            move_to=move_to,
            padding_x=padding_x,
            padding_y=padding_y,
            style=style,
            visible=visible,
            disabled=disabled,
        )

        self.inside = QWidget(parent)
        self.inside.setFixedSize(inner_width, inner_height)
        self.setWidget(self.inside)
