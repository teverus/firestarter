from PyQt5.QtWidgets import QPushButton

from Code.TeverusSDK.Widgets.BaseWidget import BaseWidget, Font


class Button(QPushButton, BaseWidget):
    def __init__(
        self,
        parent,
        width,
        height,
        padding_x=0,
        padding_y=0,
        right_from=None,
        under=None,
        move_to=None,
        text=None,
        align=None,
        font=Font.REGULAR,
        icon=None,
        icon_size=36,
        style=None,
        clicked=None,
        args: list = None,
        visible=True,
        disabled=False,
        record_to=None,
        record_as=None,
        show_dimensions=False
    ):
        super().__init__(
            parent,
            width,
            height,
            padding_x=padding_x,
            padding_y=padding_y,
            right_from=right_from,
            under=under,
            move_to=move_to,
            text=text,
            font=font,
            icon=icon,
            icon_size=icon_size,
            style=style,
            clicked=clicked,
            args=[args] if args and not isinstance(args, list) else args,
            visible=visible,
            disabled=disabled,
            align=align,
            record_to=record_to,
            record_as=record_as,
            show_dimensions=show_dimensions
        )
