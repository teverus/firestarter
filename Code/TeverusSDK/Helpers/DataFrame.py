import pandas as pd


class DataFrame:
    @classmethod
    def read_from_sql(cls, table_name, connection):
        sql_request = f"select * from {table_name}"
        result = pd.read_sql(sql_request, connection)

        return result
