from pathlib import Path

import yaml

SETTINGS = "Settings"


class YamlTool:
    @classmethod
    def create_a_settings_file(cls, path: str, file_name: str):
        """
        :param path: Path to directory
        :param file_name: Only file name, no extension!
        """
        file_name_full = f"{file_name}.yaml"
        file_path = Path(path) / file_name_full
        file_path.touch(exist_ok=True)
        data = {SETTINGS: {"hello": "world"}}
        with open(file_path, "w") as file:
            yaml.dump(data, file, sort_keys=False)

    def __init__(self, path_to_config_file: str):
        self.path_to_config_file = Path(path_to_config_file)

    def get_config(self):
        with open(self.path_to_config_file, "r") as file:
            data = yaml.safe_load(file)

        return data

    def get_settings(self):
        config = self.get_config()
        settings = config[SETTINGS]

        return settings

    def save_settings(self, data: dict):
        with open(self.path_to_config_file, "w") as file:
            yaml.dump(data, file, sort_keys=False, allow_unicode=True)

    def update_a_setting(self, setting_name, new_value, config_section=SETTINGS):
        config = self.get_config()
        settings = config[config_section]
        settings[setting_name] = new_value
        self.save_settings(config)
