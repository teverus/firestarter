from datetime import datetime


class TimeHelpers:
    @staticmethod
    def get_current_time():
        time_now = datetime.now()
        result = str(time_now).split(".")[0]

        return result
