import sqlite3
from typing import Union, List


# noinspection SqlNoDataSourceInspection
class DataBase:
    def __init__(
        self,
        path_to_database: str,
        default_table: str = None,
        default_column: str = None,
    ):
        self.connection = sqlite3.connect(path_to_database)
        self.cursor = self.connection.cursor()
        self.default_table = default_table
        self.default_column = default_column

    def execute_request(self, sql_request, commit=False, fetch_all=False):
        if fetch_all:
            result = self.cursor.execute(sql_request).fetchall()
        else:
            result = self.cursor.execute(sql_request)

        if commit:
            self.connection.commit()

        return result

    # === C: WRITE =====================================================================

    def write_to_db(
        self,
        new_values: list,
        target_table: str = None,
    ):
        table = self.default_table if self.default_table else target_table
        table = target_table if target_table else table
        data = tuple(new_values) if not isinstance(new_values, tuple) else new_values
        sql_request = f"insert into {table} values {data}"

        self.cursor.execute(sql_request)
        self.connection.commit()

    def change_in_db(self, target_table: str, column_name: str, new_values: list):
        ...

    # === R: READ ======================================================================

    def read_table(
        self,
        target_table: str = None,
    ) -> List[tuple]:
        table = self.default_table if self.default_table else target_table
        sql_request = f"select * from {table}"
        result = self.cursor.execute(sql_request).fetchall()

        return result

    def find_in_db(
        self,
        target_value: Union[str, int, float],
        target_table: str = None,
        column_name: str = None,
        single_tuple=False,
    ) -> List[tuple]:
        table = self.default_table if self.default_table else target_table
        column = self.default_column if self.default_column else column_name

        request = f"select * from {table} where {column} = '{target_value}'"
        result = self.cursor.execute(request).fetchall()

        if single_tuple:
            assert len(result) == 1
            result = result[0]

        return result

    # === U: UPDATE ====================================================================
    def update_value(
        self,
        target_column,
        new_value,
        target_table=None,
        search_by_column=None,
        search_by_value=None,
    ):
        table = self.default_table if self.default_table else target_table
        column = self.default_column if self.default_column else target_column

        request = f"update {table} set {column} = '{new_value}'"
        if search_by_column and search_by_value:
            request = f"{request} where {search_by_column} = '{search_by_value}'"

        self.cursor.execute(request)
        self.connection.commit()

    # === D: DELETE ====================================================================
    def delete_in_db(
        self,
        find_by_value: Union[str, int, float],
        target_table: str = None,
        column_name: str = None,
    ):
        table = self.default_table if self.default_table else target_table
        column = self.default_column if self.default_column else column_name

        request = f"delete from {table} where {column} = '{find_by_value}'"
        result = self.cursor.execute(request)
        self.connection.commit()

        return result
