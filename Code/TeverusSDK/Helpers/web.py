import requests


class WebHelpers:
    @staticmethod
    def save_from_web(target_url, target_path):
        response = requests.get(target_url).content
        with open(target_path, "wb") as file:
            file.write(response)
