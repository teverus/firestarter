from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtWidgets import QWidget


class BaseWorker(QThread):
    """
    BaseWorker class consists of two parts:
    * custom signals
    * connecting custom signals to real slots
    * __set_visible is arbitrary, setVisible - is set
    * -----------------------------------------------
    * A worker class must iherit from BaseWorker
    * In the worker class you must use signals to work with objects
    """

    __clear = pyqtSignal(QWidget)
    __clear_focus = pyqtSignal(QWidget)
    __exec_ = pyqtSignal(QWidget)
    __set_disabled = pyqtSignal(QWidget, bool)
    __set_focus = pyqtSignal(QWidget)
    __set_icon = pyqtSignal(QWidget, str)
    __set_icon_int = pyqtSignal(QWidget, int)
    __set_pixmap = pyqtSignal(QWidget, QPixmap)
    __set_style_sheet = pyqtSignal(QWidget, str)
    __set_text = pyqtSignal(QWidget, str)
    __set_visible = pyqtSignal(QWidget, bool)

    def __init__(self):
        super(BaseWorker, self).__init__()

        # === Actual methods ===========================================================

        self.__clear.connect(lambda widget: widget.clear())
        self.__clear_focus.connect(lambda widget: widget.clearFocus())
        self.__exec_.connect(lambda widget: widget.exec_())
        self.__set_disabled.connect(lambda widget, state: widget.setDisabled(state))
        self.__set_focus.connect(lambda widget: widget.setFocus())
        self.__set_icon.connect(lambda widget, icon: widget.setIcon(QIcon(icon)))
        self.__set_icon_int.connect(lambda widget, icon_int: widget.setIcon(icon_int))
        self.__set_pixmap.connect(lambda widget, image: widget.setPixmap(image))
        self.__set_style_sheet.connect(lambda widget, s: widget.setStyleSheet(s))
        self.__set_text.connect(lambda widget, text: widget.setText(text))
        self.__set_visible.connect(lambda widget, state: widget.setVisible(state))

        # === Nice methods to be used in a worker ======================================

        self.clear = self.__clear.emit
        self.clear_focus = self.__clear_focus.emit
        self.exec_ = self.__exec_.emit
        self.set_disabled = self.__set_disabled.emit
        self.set_focus = self.__set_focus.emit
        self.set_icon = self.__set_icon.emit
        self.set_icon_int = self.__set_icon_int.emit
        self.set_pixmap = self.__set_pixmap.emit
        self.set_style_sheet = self.__set_style_sheet.emit
        self.set_text = self.__set_text.emit
        self.set_visible = self.__set_visible.emit

