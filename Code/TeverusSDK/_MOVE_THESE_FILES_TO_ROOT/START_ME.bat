@echo off
mode con: cols=120 lines=43
title=Application

: ======================================================================================
: Installing virtual environment
: ======================================================================================
: --- Variables ---
set venv_name=venv
set packages_file=%venv_name%\packages_installed.txt

: --- Check if virtual environment exists ---
if exist %venv_name%\ (
    echo Virtual environment : Exists
) else (
    echo Virtual environment : Created
    py -m venv %venv_name%
)

: --- Activating virtual environment ---
echo Virtual environment : Activated
call %venv_name%\Scripts\activate

: --- Installing packages ---
if exist %packages_file% (
    echo Required packages   : Found
) else (
    echo Required packages   : Installed
    pip install -r requirements.txt 1>nul 2>nul
    type nul > %packages_file%
)


: ======================================================================================
: Starting the application
: ======================================================================================
py main.py

: ======================================================================================
: Uncomment to see tracebacks
: ======================================================================================
: timeout -1