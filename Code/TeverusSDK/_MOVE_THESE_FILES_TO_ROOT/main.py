from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QStyleFactory, QWidget

from Code.constants import WINDOW_WIDTH, WINDOW_HEIGHT


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.set_window()

    # === HELPERS ======================================================================

    def set_window(self):
        self.setWindowTitle("Название приложения")
        self.setWindowIcon(QIcon("Files/Icons/favicon.ico"))
        self.setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT)
        self.show()


if __name__ == "__main__":
    application = QApplication([])
    application.setStyle(QStyleFactory.create("Fusion"))
    window = MainWindow()
    application.exec_()
