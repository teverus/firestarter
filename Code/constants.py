from pathlib import Path

WINDOW_WIDTH = 1616
WINDOW_HEIGHT = 1016
PADDING_BIG = 8
PADDING_SMALL = int(PADDING_BIG / 2)
PADDING_MICRO = int(PADDING_SMALL / 2)

DB_PATH = Path("Files/database.db")


class OSNameBackend:
    AltE = "AltE"
    AltW = "AltW"
    AltWK = "AltWK"
    Astra = "Astra"
    Windows78 = "Windows78"
    Windows1011 = "Windows1011"
    WindowsNET = "WindowsNET"


class OSNameFrontend:
    AltE = "AltE"
    AltW = "AltW"
    AltWK = "AltW-*-K"
    Astra = "Astra"
    Windows78 = "Windows 7-8"
    Windows1011 = "Windows 10-11"
    WindowsNET = "Windows .NET"


BACKEND_OS_NAMES = {
    OSNameFrontend.AltE: OSNameBackend.AltE,
    OSNameFrontend.AltW: OSNameBackend.AltW,
    OSNameFrontend.AltWK: OSNameBackend.AltWK,
    OSNameFrontend.Astra: OSNameBackend.Astra,
    OSNameFrontend.Windows78: OSNameBackend.Windows78,
    OSNameFrontend.Windows1011: OSNameBackend.Windows1011,
    OSNameFrontend.WindowsNET: OSNameBackend.WindowsNET,
}

FRONTEND_OS_NAMES = {
    OSNameBackend.AltE: OSNameFrontend.AltE,
    OSNameBackend.AltW: OSNameFrontend.AltW,
    OSNameBackend.AltWK: OSNameFrontend.AltWK,
    OSNameBackend.Astra: OSNameFrontend.Astra,
    OSNameBackend.Windows78: OSNameFrontend.Windows78,
    OSNameBackend.Windows1011: OSNameFrontend.Windows1011,
    OSNameBackend.WindowsNET: OSNameFrontend.WindowsNET,
}


class Settings:
    LOGIN = "login"
    PASSWORD = "password"


# region Database


class Table:
    MIN_OS = "min_os"
    LEFT_FRAME = "left_frame"


class MinOSColumns:
    OS_NAME = "OS_name"
    RUN_URL = "run_url"
    REPORT_URL = "report_url"


class LeftFrameColumns:
    KEY = "key"
    VALUE = "value"


class RunSettings:
    BRANCH = "branch"
    TESTS = "tests"


# endregion
