from Code.TeverusSDK.Workers.BaseWorker import BaseWorker


class PrettyInputWorker(BaseWorker):
    def __init__(self, widget):
        super().__init__()
        self.widget = widget

    def run(self):
        current_text = self.widget.text()

        if not current_text.startswith(" "):
            self.set_text(self.widget, f" {current_text}")

        if current_text and current_text != " ":
            self.set_style_sheet(self.widget, "")
        else:
            self.set_text(self.widget, " ")
            self.set_style_sheet(self.widget, "background: pink")
