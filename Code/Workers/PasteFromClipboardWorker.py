import pyperclip as pc
from Code.TeverusSDK.Workers.BaseWorker import BaseWorker


class PasteFromClipboardWorker(BaseWorker):
    def __init__(self, widget):
        super().__init__()
        self.widget = widget

    def run(self):
        new_text = pc.paste()
        self.set_text(self.widget, new_text)
