from Code.TeverusSDK.Workers.BaseWorker import BaseWorker


class SetButtonStateBasedOnInputWorker(BaseWorker):
    def __init__(self, input_widget, button):
        super().__init__()
        self.input_widget = input_widget
        self.button = button

    def run(self):
        text = self.input_widget.text()
        is_disabled = False if text and text != " " else True

        self.set_disabled(self.button, is_disabled)
