import pyperclip as pc
from Code.TeverusSDK.Workers.BaseWorker import BaseWorker


class CopyWidgetTextWorker(BaseWorker):
    def __init__(self, source_widget, strip=True):
        super().__init__()
        self.source_widget = source_widget
        self.strip = strip

    def run(self):
        text = self.source_widget.text()

        text = text.strip() if self.strip else text

        pc.copy(text)
