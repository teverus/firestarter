from Code.TeverusSDK.Workers.BaseWorker import BaseWorker


class ClearInputsWorker(BaseWorker):
    def __init__(self, main):
        super().__init__()
        self.main = main

    def run(self):
        self.set_disabled(self.main.clear_all_button, True)

        for index in range(1, 8):
            widget = self.main.__getattribute__(f"min_slot_{index}")
            run_input = widget.run_input
            report_input = widget.report_input

            self.clear(run_input)
            self.clear(report_input)

        self.set_disabled(self.main.clear_all_button, False)
