import webbrowser

from Code.TeverusSDK.Workers.BaseWorker import BaseWorker


class OpenInBrowserWorker(BaseWorker):
    def __init__(self, label_input):
        super().__init__()
        self.label_input = label_input

    def run(self):
        url = self.label_input.text().strip()
        webbrowser.open_new(url)
