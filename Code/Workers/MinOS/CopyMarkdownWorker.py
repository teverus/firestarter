import pyperclip as pc

from Code.DTOs.MinOsSlotDTO import MinOsSlotDTO
from Code.TeverusSDK.Helpers.DataBase import DataBase
from Code.TeverusSDK.Workers.BaseWorker import BaseWorker
from Code.constants import Table, DB_PATH, FRONTEND_OS_NAMES as FE


# noinspection PyAttributeOutsideInit
class CopyMarkdownWorker(BaseWorker):
    def __init__(self, main):
        super().__init__()
        self.main = main

    def run(self):
        self.database = DataBase(DB_PATH, default_table=Table.MIN_OS)
        known_data = [MinOsSlotDTO(*d) for d in self.database.read_table()]
        run_stats = [
            f"[прогон]({e.run_input}) | [отчет]({e.report_input}) <- **{FE[e.name]}**"
            for e in known_data
        ]
        run_stats_formatted = "\n".join(run_stats)
        branch_name = self.main.branch_input.text().strip()
        tests = "\n* ".join(sorted(self.main.tests_input.text().strip().split(" ")))

        result = (
            f"## Задачка\n"
            f"{branch_name}\n"
            f"## Затронутые тесты\n"
            f"* {tests}\n"
            f"## Прогоны\n"
            f"{run_stats_formatted}\n"
            f"## Что было сделано\n"
            f"* "
        )

        pc.copy(result)

        # TODO Тут надо дергать MessageBox, мол, все скопировалось...
