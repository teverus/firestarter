from Code.TeverusSDK.Helpers.DataBase import DataBase
from Code.TeverusSDK.Workers.BaseWorker import BaseWorker
from Code.constants import MinOSColumns, DB_PATH, Table
from Code.helpers import get_backend_os_name


class UpdateOsUrlWorker(BaseWorker):
    def __init__(self, input_widget, prefix, name):
        super().__init__()
        self.input_widget = input_widget
        self.prefix = prefix
        self.name = name

    def run(self):
        text = self.input_widget.text()
        os_name = self.name.split(" | ")[0]
        backend_os_name = get_backend_os_name(os_name)
        proper_url_name = f"{self.prefix}_url"
        database = DataBase(DB_PATH, default_table=Table.MIN_OS)

        # To make text input look pretty
        if text and not text.startswith(" "):
            self.set_text(self.input_widget, f" {self.input_widget.text()}")

        if text and text != " ":
            self.set_style_sheet(self.input_widget, "")
        else:
            self.set_text(self.input_widget, " ")
            self.set_style_sheet(self.input_widget, "background: pink")

        database.update_value(
            target_column=proper_url_name,
            new_value=text.strip(),
            search_by_column=MinOSColumns.OS_NAME,
            search_by_value=backend_os_name,
        )
