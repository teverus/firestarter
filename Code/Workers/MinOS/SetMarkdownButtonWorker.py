from Code.TeverusSDK.Workers.BaseWorker import BaseWorker


class SetMarkdownButtonWorker(BaseWorker):
    def __init__(self, main):
        super().__init__()
        self.main = main

    def run(self):
        run_inputs_text = [
            self.main.__getattribute__(f"min_slot_{index}").run_input.text().strip()
            for index in range(1, 8)
        ]

        report_inputs_text = [
            self.main.__getattribute__(f"min_slot_{index}").report_input.text().strip()
            for index in range(1, 8)
        ]

        run_inputs_filled = all(run_inputs_text)
        report_inputs_filled = all(report_inputs_text)

        status = False if all([run_inputs_filled, report_inputs_filled]) else True

        self.set_disabled(self.main.copy_to_markdown_button, status)
