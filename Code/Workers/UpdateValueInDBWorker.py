from Code.TeverusSDK.Helpers.DataBase import DataBase
from Code.TeverusSDK.Workers.BaseWorker import BaseWorker
from Code.constants import DB_PATH


class UpdateValueInDBWorker(BaseWorker):
    def __init__(
        self,
        table,
        column,
        new_value,
        search_by_column=None,
        search_by_value=None,
    ):
        super().__init__()
        self.table = table
        self.column = column
        self.new_value = new_value
        self.search_by_column = search_by_column
        self.search_by_value = search_by_value

    def run(self):
        database = DataBase(DB_PATH, default_table=self.table)

        database.update_value(
            target_column=self.column,
            new_value=self.new_value,
            search_by_column=self.search_by_column,
            search_by_value=self.search_by_value
        )
