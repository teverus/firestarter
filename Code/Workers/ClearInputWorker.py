from Code.TeverusSDK.Workers.BaseWorker import BaseWorker


class ClearInputWorker(BaseWorker):
    def __init__(self, widget, remove_focus=True):
        super().__init__()
        self.widget = widget
        self.remove_focus = remove_focus

    def run(self):
        self.set_text(self.widget, " ")

        if self.remove_focus:
            self.set_focus(self.widget)
            self.clear_focus(self.widget)
