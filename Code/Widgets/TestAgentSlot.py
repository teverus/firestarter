import pyperclip as pc
from Code.TeverusSDK.Widgets.BaseWidget import Font
from Code.TeverusSDK.Widgets.Button import Button
from Code.TeverusSDK.Widgets.Label import Label
from Code.constants import PADDING_SMALL


class TestAgentSlot:
    def __init__(
        self,
        main,
        prefix,
        name,
        move_to=None,
        under=None,
        padding_y=0,
        background_color=None,
        extra_background_height=0,
        extra_first_row_height=0,
    ):
        self.main = main
        self.agents_all = self.main.agents_all
        self.agents_this = [e for e in self.agents_all if e.agent == name]
        self.prefix = prefix
        self.name = name
        self.background_color = background_color

        self.parent = self.main.test_agents_tab

        self.background = Label(
            self.main.test_agents_tab,
            width=self.main.ta_back.width(),
            height=self.main.ta_row_height + extra_background_height,
            move_to=move_to,
            under=under,
            padding_y=padding_y,
            record_to=self.main,
            record_as=f"{prefix}_back",
        )
        self.background_color = Label(
            self.main.test_agents_tab,
            width=self.background.width() - (PADDING_SMALL * 3),
            height=self.background.height(),
            move_to=self.background,
            padding_x=PADDING_SMALL,
            style=f"background: {background_color}",
        )

        self.button_1 = Label(
            self.main.test_agents_tab,
            width=self.main.agent_width,
            height=self.main.agent_height + extra_first_row_height,
            move_to=self.background,
            padding_x=PADDING_SMALL,
            padding_y=PADDING_SMALL,
            show_dimensions=True,
            text=self.name,
            font=Font.REGULAR_BOLD_BIGGER,
        )

        self.button_2 = Button(
            self.main.test_agents_tab,
            width=self.button_1.width(),
            height=self.button_1.height(),
            right_from=self.button_1,
            show_dimensions=True,
            padding_x=PADDING_SMALL,
            visible=self.check_if_visible(1),
            text=self.get_name(1),
            style=self.get_style(1),
            clicked=self.copy_test_agent_name,
            args=[1]
        )

        self.button_3 = Button(
            self.main.test_agents_tab,
            width=self.button_1.width() + 1,
            height=self.button_1.height(),
            right_from=self.button_2,
            show_dimensions=True,
            padding_x=PADDING_SMALL,
            visible=self.check_if_visible(2),
            text=self.get_name(2),
            style=self.get_style(2),
            clicked=self.copy_test_agent_name,
            args=[2]
        )

        self.button_4 = Button(
            self.main.test_agents_tab,
            width=self.button_1.width() + 1,
            height=self.button_1.height(),
            right_from=self.button_3,
            show_dimensions=True,
            padding_x=PADDING_SMALL,
            visible=self.check_if_visible(3),
            text=self.get_name(3),
            style=self.get_style(3),
            clicked=self.copy_test_agent_name,
            args=[3]
        )

        self.button_5 = Button(
            self.main.test_agents_tab,
            width=self.button_1.width() + 1,
            height=self.button_1.height(),
            right_from=self.button_4,
            show_dimensions=True,
            padding_x=PADDING_SMALL,
            visible=self.check_if_visible(4),
            text=self.get_name(4),
            style=self.get_style(4),
            clicked=self.copy_test_agent_name,
            args=[4]
        )

        self.button_6 = Button(
            self.main.test_agents_tab,
            width=self.main.agent_width,
            height=self.main.agent_height + 1,
            under=self.button_1,
            padding_y=PADDING_SMALL,
            show_dimensions=True,
            visible=self.check_if_visible(5),
            text=self.get_name(5),
            style=self.get_style(5),
            clicked=self.copy_test_agent_name,
            args=[5]
        )

        self.button_7 = Button(
            self.main.test_agents_tab,
            width=self.button_6.width(),
            height=self.button_6.height(),
            right_from=self.button_6,
            padding_x=PADDING_SMALL,
            show_dimensions=True,
            visible=self.check_if_visible(6),
            text=self.get_name(6),
            style=self.get_style(6),
            clicked=self.copy_test_agent_name,
            args=[6]
        )

        self.button_8 = Button(
            self.main.test_agents_tab,
            width=self.button_6.width() + 1,
            height=self.button_6.height(),
            right_from=self.button_7,
            padding_x=PADDING_SMALL,
            show_dimensions=True,
            visible=self.check_if_visible(7),
            text=self.get_name(7),
            style=self.get_style(7),
            clicked=self.copy_test_agent_name,
            args=[7]
        )

        self.button_9 = Button(
            self.main.test_agents_tab,
            width=self.button_6.width() + 1,
            height=self.button_6.height(),
            right_from=self.button_8,
            padding_x=PADDING_SMALL,
            show_dimensions=True,
            visible=self.check_if_visible(8),
            text=self.get_name(8),
            style=self.get_style(8),
            clicked=self.copy_test_agent_name,
            args=[8]
        )

        self.button_10 = Button(
            self.main.test_agents_tab,
            width=self.button_6.width() + 1,
            height=self.button_6.height(),
            right_from=self.button_9,
            padding_x=PADDING_SMALL,
            show_dimensions=True,
            visible=self.check_if_visible(9),
            text=self.get_name(9),
            style=self.get_style(9),
            clicked=self.copy_test_agent_name,
            args=[9]
        )

    def check_if_visible(self, target_index):
        number_of_agents = len(self.agents_this)

        result = False if target_index > number_of_agents else True

        if self.main.agents_total_row_number < number_of_agents:
            raise Exception(f"You need show at least {number_of_agents} agents!")
        else:
            return result

    def get_name(self, target_index):
        index = target_index - 1
        try:
            info = self.agents_this[index]
        except IndexError:
            return ""

        try:
            result = f"[{info.agent_raw}]\n{', '.join(info.names)}"
        except TypeError:
            result = f"[{info.agent_raw}]"

        return result

    def get_style(self, target_index):
        try:
            info = self.agents_this[target_index - 1]
        except IndexError:
            return ""

        color = "red" if info.names else "LightGreen"
        color = "orange" if info.logins and self.main.login in info.logins else color

        return f"background: {color}"

    # region --- WORKERS ---------------------------------------------------------------
    def copy_test_agent_name(self, index):
        widget = self.__getattribute__(f"button_{index + 1}")
        text = widget.text().split("\n")[0].strip("[]")

        pc.copy(text)

    # endregion