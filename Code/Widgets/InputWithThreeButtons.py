from Code.TeverusSDK.Helpers.DataBase import DataBase
from Code.TeverusSDK.Widgets.BaseWidget import Font
from Code.TeverusSDK.Widgets.Button import Button
from Code.TeverusSDK.Widgets.Input import Input
from Code.TeverusSDK.Widgets.Label import Label
from Code.Workers.ClearInputWorker import ClearInputWorker
from Code.Workers.CopyWidgetTextWorker import CopyWidgetTextWorker
from Code.Workers.PasteFromClipboardWorker import PasteFromClipboardWorker
from Code.Workers.PrettyInputWorker import PrettyInputWorker
from Code.Workers.SetButtonStateBasedOnInputWorker import (
    SetButtonStateBasedOnInputWorker,
)
from Code.Workers.UpdateValueInDBWorker import UpdateValueInDBWorker
from Code.constants import (
    PADDING_SMALL,
    DB_PATH,
    Table,
    LeftFrameColumns,
    RunSettings,
    PADDING_MICRO,
)


# noinspection PyAttributeOutsideInit
class InputWithThreeButtons:
    def __init__(self, main, prefix, input_title, under=None):
        self.main = main
        self.prefix = prefix
        self.parent = self.main.left_frame
        self.input_value = self.get_input_value_from_db()
        self.number_of_buttons = 3
        self.button_width = int(self.parent.width() / self.number_of_buttons)
        self.button_height = self.main.left_row_height

        self.label = Label(
            self.parent,
            width=self.parent.width(),
            height=self.button_height,
            under=under,
            text=input_title,
            font=Font.TITLE,
            record_to=self.main,
            record_as=f"{prefix}_label",
        )

        self.input = Input(
            self.parent,
            width=self.parent.width(),
            height=self.button_height - PADDING_SMALL,
            text=f" {self.input_value}" if self.input_value else "",
            under=self.label,
            record_to=self.main,
            record_as=f"{prefix}_input",
            style="" if self.input_value.strip() else "background: pink",
            text_changed=self.fill_in_input,
        )

        self.left_button = Button(
            self.parent,
            width=self.button_width - PADDING_SMALL + 1,
            height=self.button_height,
            under=self.input,
            padding_y=PADDING_SMALL,
            icon="Files/Icons/copy.png",
            record_to=self.main,
            record_as=f"{prefix}_left_button",
            disabled=False if self.input_value.strip() else True,
            clicked=self.copy_input_text,
        )

        self.middle_button = Button(
            self.parent,
            width=self.button_width - PADDING_SMALL + 1,
            height=self.button_height,
            right_from=self.left_button,
            padding_x=PADDING_SMALL,
            icon="Files/Icons/paste.png",
            record_to=self.main,
            record_as=f"{prefix}_middle_button",
            clicked=self.paste_branch_name,
        )

        self.right_button = Button(
            self.parent,
            width=self.button_width - PADDING_MICRO,
            height=self.button_height,
            right_from=self.middle_button,
            padding_x=PADDING_SMALL,
            icon="Files/Icons/clear.png",
            record_to=self.main,
            record_as=f"{prefix}_right_button",
            disabled=False if self.input_value.strip() else True,
            clicked=self.clear_branch_input,
        )

    # region === WORKERS ===============================================================

    def fill_in_input(self):
        cp = self.left_button
        cl = self.right_button
        input_ = self.input
        new_text = input_.text()
        current_text = self.get_input_value_from_db()

        if new_text.strip() != current_text.strip():
            self.pretty_input_worker = PrettyInputWorker(input_)
            self.copy_button_worker = SetButtonStateBasedOnInputWorker(input_, cp)
            self.clear_button_worker = SetButtonStateBasedOnInputWorker(input_, cl)
            self.db = UpdateValueInDBWorker(
                table=Table.LEFT_FRAME,
                column=LeftFrameColumns.VALUE,
                new_value=new_text,
                search_by_column=LeftFrameColumns.KEY,
                search_by_value=self.prefix,
            )

            for worker in [
                self.pretty_input_worker,
                self.copy_button_worker,
                self.clear_button_worker,
                self.db,
            ]:
                worker.start()
                worker.wait()

    def paste_branch_name(self):
        # TODO Append paste and clear paste
        self.paste_worker = PasteFromClipboardWorker(self.input)
        self.paste_worker.start()
        self.paste_worker.wait()

    def clear_branch_input(self):
        self.clear_worker = ClearInputWorker(self.input)
        self.clear_worker.start()
        self.clear_worker.wait()

    def copy_input_text(self):
        self.copy_worker = CopyWidgetTextWorker(self.input)
        self.copy_worker.start()
        self.copy_worker.wait()

    # endregion ========================================================================

    # region --- HELPERS ---------------------------------------------------------------

    def get_input_value_from_db(self):
        data_known_all = DataBase(DB_PATH, default_table=Table.LEFT_FRAME).read_table()
        data_as_dict = {e[0]: e[1] for e in data_known_all}
        result = data_as_dict[self.prefix]

        return result

    # endregion
