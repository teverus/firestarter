from Code.TeverusSDK.Helpers.DataBase import DataBase
from Code.TeverusSDK.Widgets.BaseWidget import Font
from Code.TeverusSDK.Widgets.Button import Button
from Code.TeverusSDK.Widgets.Input import Input
from Code.TeverusSDK.Widgets.Label import Label
from Code.Workers.SetButtonStateBasedOnInputWorker import SetButtonStateBasedOnInputWorker
from Code.Workers.ClearInputWorker import ClearInputWorker
from Code.Workers.CopyWidgetTextWorker import CopyWidgetTextWorker
from Code.Workers.MinOS.OpenInBrowserWorker import OpenInBrowserWorker
from Code.Workers.PasteFromClipboardWorker import PasteFromClipboardWorker
from Code.Workers.MinOS.UpdateOsUrlWorker import UpdateOsUrlWorker
from Code.constants import (
    PADDING_SMALL,
    PADDING_BIG,
    DB_PATH,
    Table,
    MinOSColumns,
)
from Code.helpers import get_backend_os_name

# TODO выключать кнопку Удалить всё, если ни один ввод не заполнен
# TODO выключать кнопку маркдауна, если не заполнена ветка
# TODO выключать кнопку маркдауна, если не заполнены тесты
# TODO выключать кнопку маркдауна, если отсутствует тест агент

# noinspection PyAttributeOutsideInit
class MinOsSlot:
    def __init__(
        self,
        parent,
        name,
        slot_width,
        slot_height,
        right_from=None,
        under=None,
        padding_x=None,
        padding_y=None,
        style=None
    ):
        self.main = parent
        self.parent = parent.min_os_tab
        self.name = name
        self.slot_width = slot_width
        self.slot_height = slot_height
        self.number_of_rows = 5
        self.row_height = int(self.slot_height / self.number_of_rows)
        self.button_width = int(slot_width / 4)

        self.slot_name = Label(
            self.parent,
            width=slot_width,
            height=self.row_height,
            right_from=right_from or None,
            under=under or None,
            padding_x=padding_x or 0,
            padding_y=padding_y or 0,
            text=self.name,
            show_dimensions=True,
            font=Font.REGULAR_BOLD_BIGGER,
            style=style
        )

        self.create_input_and_buttons(
            input_name="Прогон",
            prefix="run",
        )
        self.create_input_and_buttons(
            input_name="Отчет",
            prefix="report",
            under=self.run_copy_button,
        )

    # === ACTIONS ======================================================================

    def create_input_and_buttons(self, input_name, prefix, under=None):
        label_attribute_name = f"{prefix}_label"

        copy_button_attribute_name = f"{prefix}_copy_button"
        paste_button_attribute_name = f"{prefix}_paste_button"
        clear_button_attribute_name = f"{prefix}_clear_button"
        open_button_attribute_name = f"{prefix}_open_button"

        database = DataBase(DB_PATH, Table.MIN_OS)
        backend_os_name = get_backend_os_name(self.name)
        data = database.find_in_db(
            target_value=backend_os_name,
            column_name=MinOSColumns.OS_NAME,
            single_tuple=True,
        )
        index = 1 if prefix == "run" else 2
        known_url = data[index]

        label = Label(
            self.parent,
            width=int(self.slot_width * 0.15),
            height=self.row_height,
            under=under if under else self.slot_name,
            padding_x=-PADDING_SMALL if under else 0,
            text=input_name,
            show_dimensions=True,
            font=Font.REGULAR_BOLD,
        )
        self.__setattr__(label_attribute_name, label)

        label_input = Input(
            self.parent,
            width=int(self.slot_width * 0.85) - PADDING_SMALL + 1,
            height=self.row_height - PADDING_BIG,
            padding_y=PADDING_SMALL,
            right_from=self.__getattribute__(label_attribute_name),
            text=f" {known_url}",
            style="" if known_url else "background: pink",
        )
        self.__setattr__(f"{prefix}_input", label_input)

        copy_button = Button(
            self.parent,
            width=self.button_width - PADDING_BIG,
            height=self.row_height,
            padding_x=PADDING_SMALL,
            under=self.__getattribute__(label_attribute_name),
            icon="Files/Icons/copy.png",
            clicked=self.copy_from_clipboard,
            args=label_input,
            disabled=False if known_url else True,
        )
        self.__setattr__(copy_button_attribute_name, copy_button)

        paste_button = Button(
            self.parent,
            width=self.button_width - PADDING_BIG,
            height=self.row_height,
            right_from=self.__getattribute__(copy_button_attribute_name),
            padding_x=PADDING_SMALL,
            icon="Files/Icons/paste.png",
            clicked=self.paste_from_clipboard,
            args=label_input,
        )
        self.__setattr__(paste_button_attribute_name, paste_button)

        open_button = Button(
            self.parent,
            width=self.button_width - 4,
            height=self.row_height,
            right_from=self.__getattribute__(paste_button_attribute_name),
            padding_x=PADDING_SMALL,
            icon="Files/Icons/open_in_web.png",
            disabled=False if known_url else True,
            clicked=self.open_in_browser,
            args=label_input,
        )
        self.__setattr__(open_button_attribute_name, open_button)

        clear_button = Button(
            self.parent,
            width=self.button_width,
            height=self.row_height,
            right_from=self.__getattribute__(open_button_attribute_name),
            padding_x=PADDING_SMALL,
            icon="Files/Icons/clear.png",
            clicked=self.clear_input,
            args=label_input,
            disabled=False if known_url else True,
        )
        self.__setattr__(clear_button_attribute_name, clear_button)

        # ------------------------------------------------------------------------------

        label_input.textChanged.connect(
            lambda: self.fill_input(
                label_input,
                prefix,
                copy_button,
                clear_button,
                open_button,
            )
        )

    # === WORKERS ======================================================================

    def fill_input(self, input_widget, prefix, copy_button, clear_button, open_button):
        cp = copy_button
        cl = clear_button
        ob = open_button

        self.update_os_url_worker = UpdateOsUrlWorker(input_widget, prefix, self.name)
        self.copy_button_state_worker = SetButtonStateBasedOnInputWorker(input_widget, cp)
        self.paste_button_state_worker = SetButtonStateBasedOnInputWorker(input_widget, cl)
        self.open_button_state_worker = SetButtonStateBasedOnInputWorker(input_widget, ob)

        for worker in [
            self.update_os_url_worker,
            self.copy_button_state_worker,
            self.paste_button_state_worker,
            self.open_button_state_worker,
        ]:
            worker.start()
            worker.wait()

        self.main.set_markdown_button()

    def paste_from_clipboard(self, label_input):
        self.paste_from_clipboard_worker = PasteFromClipboardWorker(label_input)
        self.paste_from_clipboard_worker.start()
        self.paste_from_clipboard_worker.wait()

    def copy_from_clipboard(self, label_input):
        self.copy_from_clipboard_worker = CopyWidgetTextWorker(label_input)
        self.copy_from_clipboard_worker.start()
        self.copy_from_clipboard_worker.wait()

    def clear_input(self, label_input):
        self.clear_input_worker = ClearInputWorker(label_input)
        self.clear_input_worker.start()
        self.clear_input_worker.wait()

    def open_in_browser(self, label_input):
        self.open_in_browser_worker = OpenInBrowserWorker(label_input)
        self.open_in_browser_worker.start()
        self.open_in_browser_worker.wait()
