from Code.constants import BACKEND_OS_NAMES


def get_backend_os_name(frontend_name):
    os_name = frontend_name.split(" | ")[0]
    result = BACKEND_OS_NAMES[os_name]

    return result
