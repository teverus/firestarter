from dataclasses import dataclass
from typing import List


@dataclass
class TestAgentDto:
    agent_raw: str
    agent: str
    names: List[str]
    logins: List[str]
