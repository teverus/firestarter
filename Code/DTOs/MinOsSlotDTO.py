from dataclasses import dataclass


@dataclass
class MinOsSlotDTO:
    name: str
    run_input: str
    report_input: str
