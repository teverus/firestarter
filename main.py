from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (
    QApplication,
    QStyleFactory,
    QWidget,
)

from Code.DTOs.TestAgentDto import TestAgentDto
from Code.TeverusSDK.Helpers.YamlTool import YamlTool
from Code.TeverusSDK.Widgets.Button import Button
from Code.TeverusSDK.Widgets.Frame import Frame
from Code.TeverusSDK.Widgets.Label import Label
from Code.TeverusSDK.Widgets.Tabs import Tabs
from Code.Widgets.InputWithThreeButtons import InputWithThreeButtons
from Code.Widgets.MinOsSlot import MinOsSlot
from Code.Widgets.TestAgentSlot import TestAgentSlot
from Code.Workers.MinOS.ClearInputsWorker import ClearInputsWorker
from Code.Workers.MinOS.CopyMarkdownWorker import CopyMarkdownWorker
from Code.Workers.MinOS.SetMarkdownButtonWorker import (
    SetMarkdownButtonWorker,
)
from Code.constants import (
    WINDOW_WIDTH,
    WINDOW_HEIGHT,
    PADDING_BIG,
    PADDING_SMALL,
    OSNameFrontend,
    Settings,
)
from Code.squadus_api import SquadusAPI


# noinspection PyAttributeOutsideInit
class MainWindow(QWidget):
    def __init__(self):
        super().__init__()

        # --- Variables ----------------------------------------------------------------

        self.settings = YamlTool("config.yaml").get_settings()
        self.login = self.settings[Settings.LOGIN]
        self.password = self.settings[Settings.PASSWORD]
        self.squadus_api = SquadusAPI(self.login, self.password)
        self.inventory_messages = self.squadus_api.get_inventory_messages()
        self.agents_all = self.get_agents_all()
        self.agents_my = self.get_my_agents()

        # --- Actions ------------------------------------------------------------------

        self.create_gui()
        ...

    # region CREATE GUI
    def create_gui(self):
        self.create_frames()
        self.create_left_filters()
        self.create_tabs()
        self.set_window()

        # --- Actions after GUI was shown ----------------------------------------------
        self.min_os_tab.setFocus()
        self.set_markdown_button()

    def create_frames(self):
        self.main_frame = Label(
            self,
            width=WINDOW_WIDTH - int(PADDING_BIG * 2),
            height=WINDOW_HEIGHT - int(PADDING_BIG * 2),
            padding_x=PADDING_BIG,
            padding_y=PADDING_BIG,
            text="Main frame",
        )

        self.left_frame = Label(
            self,
            width=int(self.main_frame.width() / 4) - PADDING_SMALL,
            height=self.main_frame.height(),
            move_to=self.main_frame,
        )

        self.right_frame = Label(
            self,
            width=int(self.main_frame.width() * 0.75) - PADDING_SMALL,
            height=self.main_frame.height(),
            right_from=self.left_frame,
            padding_x=PADDING_BIG,
            text="Right frame",
        )

    def create_left_filters(self):
        self.left_row_height = int(self.left_frame.height() / 20)

        InputWithThreeButtons(self, "branch", "Ветка")
        InputWithThreeButtons(self, "tests", "Тесты", self.branch_left_button)

    def create_tabs(self):
        self.tab_contents_width = self.right_frame.width()
        self.tab_contents_height = self.right_frame.height() - 40

        self.tab_contents_max_height = WINDOW_HEIGHT - 16 - 40

        self.create_min_os_tab()
        self.create_test_agents_tab()
        self.create_auto_run()

        self.tab_widget = Tabs(
            self,
            width=self.right_frame.width(),
            height=self.right_frame.height() + 1,
            move_to=self.right_frame,
            tabs={
                "[Auto] Минимальный набор ОС": self.auto_run,
                "Обзор тестовых агентов": self.test_agents_tab,
                "[1.0] Минимальный набор ОС": self.min_os_tab,

            },
        )

    def create_test_agents_tab(self):
        self.test_agents_tab = Frame(
            self,
            width=self.tab_contents_width,
            height=self.tab_contents_height,
        )

        self.ta_back = Label(
            self.test_agents_tab,
            width=self.tab_contents_width,
            height=self.tab_contents_height,
            # show_dimensions=True
        )

        self.ta_rows_number = 7
        self.ta_row_height = int(self.ta_back.height() / self.ta_rows_number)
        self.agents_number_x = 5
        self.agents_number_y = 2
        self.agents_total_row_number = int(self.agents_number_x * self.agents_number_y)
        self.agent_width = int(self.ta_back.width() / self.agents_number_x) - PADDING_SMALL - 2
        self.agent_height = int(self.ta_row_height / self.agents_number_y) - PADDING_SMALL - 2

        TestAgentSlot(
            self,
            name=OSNameFrontend.AltE,
            prefix="ta_1",
            move_to=self.ta_back,
        )
        TestAgentSlot(
            self,
            name=OSNameFrontend.AltW,
            prefix="ta_2",
            under=self.ta_1_back,
            background_color="LightGrey",
        )
        TestAgentSlot(
            self,
            name=OSNameFrontend.AltWK,
            prefix="ta_3",
            under=self.ta_2_back,
        )
        TestAgentSlot(
            self,
            name=OSNameFrontend.Astra,
            prefix="ta_4",
            under=self.ta_3_back,
            background_color="LightGrey",
        )
        TestAgentSlot(
            self,
            name=OSNameFrontend.Windows78,
            prefix="ta_5",
            under=self.ta_4_back,
        )
        TestAgentSlot(
            self,
            name=OSNameFrontend.Windows1011,
            prefix="ta_6",
            under=self.ta_5_back,
            background_color="LightGrey",
        )
        TestAgentSlot(
            self,
            name=OSNameFrontend.WindowsNET,
            prefix="ta_7",
            under=self.ta_6_back,
            extra_first_row_height=1,
            extra_background_height=1
        )

    def create_min_os_tab(self):
        # TODO проверять текст в инпутах на соответствие агенту
        # TODO деактивировать все кнопки, если нет агента
        # TODO оповещать, если больше одного тест агента в одной категории

        self.min_os_tab = Frame(
            self,
            width=self.tab_contents_width,
            height=self.tab_contents_height,
        )

        self.min_os_slot_width = int((self.right_frame.width() - 4) / 2)
        self.min_os_slot_height = int(self.tab_contents_max_height / 4)

        current_name = self.agents_my.get(OSNameFrontend.AltE, "???")
        self.min_slot_1 = MinOsSlot(
            self,
            name=f"{OSNameFrontend.AltE} | {current_name}",
            slot_width=self.min_os_slot_width,
            slot_height=self.min_os_slot_height,
            padding_y=-PADDING_SMALL,
            style="color: red" if current_name == "???" else "",
        )

        current_name = self.agents_my.get(OSNameFrontend.AltW, "???")
        self.min_slot_2 = MinOsSlot(
            self,
            name=f"{OSNameFrontend.AltW} | {current_name}",
            slot_width=self.min_os_slot_width,
            slot_height=self.min_os_slot_height,
            right_from=self.min_slot_1.slot_name,
            style="color: red" if current_name == "???" else "",
        )

        current_name = self.agents_my.get(OSNameFrontend.AltWK, "???")
        self.min_slot_3 = MinOsSlot(
            self,
            name=f"{OSNameFrontend.AltWK} | {current_name}",
            slot_width=self.min_os_slot_width,
            slot_height=self.min_os_slot_height,
            under=self.min_slot_1.report_copy_button,
            padding_x=-PADDING_SMALL,
            style="color: red" if current_name == "???" else "",
        )

        current_name = self.agents_my.get(OSNameFrontend.Astra, "???")
        self.min_slot_4 = MinOsSlot(
            self,
            name=f"{OSNameFrontend.Astra} | {current_name}",
            slot_width=self.min_os_slot_width,
            slot_height=self.min_os_slot_height,
            right_from=self.min_slot_3.slot_name,
            style="color: red" if current_name == "???" else "",
        )

        current_name = self.agents_my.get(OSNameFrontend.Windows78, "???")
        self.min_slot_5 = MinOsSlot(
            self,
            name=f"{OSNameFrontend.Windows78} | {current_name}",
            slot_width=self.min_os_slot_width,
            slot_height=self.min_os_slot_height,
            under=self.min_slot_3.report_copy_button,
            padding_x=-PADDING_SMALL,
            style="color: red" if current_name == "???" else "",
        )

        current_name = self.agents_my.get(OSNameFrontend.Windows1011, "???")
        self.min_slot_6 = MinOsSlot(
            self,
            name=f"{OSNameFrontend.Windows1011} | {current_name}",
            slot_width=self.min_os_slot_width,
            slot_height=self.min_os_slot_height,
            right_from=self.min_slot_5.slot_name,
            style="color: red" if current_name == "???" else "",
        )

        current_name = self.agents_my.get(OSNameFrontend.WindowsNET, "???")
        self.min_slot_7 = MinOsSlot(
            self,
            name=f"{OSNameFrontend.WindowsNET} | {current_name}",
            slot_width=self.min_os_slot_width,
            slot_height=self.min_os_slot_height,
            under=self.min_slot_5.report_copy_button,
            padding_x=-PADDING_SMALL,
            style="color: red" if current_name == "???" else "",
        )

        self.button_explanation_label = Label(
            self.min_os_tab,
            width=self.min_os_slot_width - PADDING_BIG,
            height=int((self.min_os_slot_height / 5) * 2) - PADDING_SMALL,
            right_from=self.min_slot_7.run_input,
            padding_x=PADDING_BIG,
            text="Эта кнопка станет активной, когда все ссылки \nна прогоны и отчеты для каждой ОС будут добавлены",
        )

        self.copy_to_markdown_button = Button(
            self.min_os_tab,
            width=int((self.min_os_slot_width - PADDING_BIG) * 0.67),
            height=int((self.min_os_slot_height / 5) * 2) - PADDING_SMALL,
            under=self.button_explanation_label,
            padding_y=PADDING_SMALL,
            text="Скопировать в красивый Markdown",
            icon="Files/Icons/markdown.png",
            disabled=True,
            style="background: LightGreen",
            clicked=self.copy_markdown,
        )

        self.clear_all_button = Button(
            self.min_os_tab,
            width=int((self.min_os_slot_width - PADDING_BIG) * 0.33) - PADDING_SMALL,
            height=int((self.min_os_slot_height / 5) * 2) - PADDING_SMALL,
            right_from=self.copy_to_markdown_button,
            padding_x=PADDING_SMALL,
            text="Удалить всё",
            icon="Files/Icons/clear.png",
            clicked=self.clear_min_os_inputs,
        )

    def create_auto_run(self):
        self.auto_run = Frame(
            self,
            width=self.tab_contents_width,
            height=self.tab_contents_height,
        )

        self.auto_run_back = Label(
            self.auto_run,
            width=self.auto_run.width(),
            height=self.auto_run.height(),
            show_dimensions=True,
            style="background: pink"

        )

        self.label_1 = Label(
            self.auto_run,
            width=self.auto_run_back.width(),
            height=int(self.auto_run_back.height() / 8),
            show_dimensions=True,
            style="background: LightGreen"
        )

        self.label_1_1 = Label(
            self.auto_run,
            width=int(self.label_1.width() / 3),
            height=int(self.label_1.height() / 1),
            move_to=self.label_1,
            show_dimensions=True,
            style="background: Pink"
        )

        self.label_1_2 = Label(
            self.auto_run,
            width=int(self.label_1.width() / 3),
            height=int(self.label_1.height() / 1),
            right_from=self.label_1_1,
            show_dimensions=True,
            style="background: LightGreen"
        )

        self.label_1_3 = Label(
            self.auto_run,
            width=int(self.label_1.width() / 3),
            height=int(self.label_1.height() / 1),
            right_from=self.label_1_2,
            show_dimensions=True,
            style="background: Orange"
        )

        self.label_2 = Label(
            self.auto_run,
            width=self.auto_run_back.width(),
            height=int(self.auto_run_back.height() / 8),
            under=self.label_1,
            show_dimensions=True,
            style="background: SkyBlue"
        )

        self.label_3 = Label(
            self.auto_run,
            width=self.auto_run_back.width(),
            height=int(self.auto_run_back.height() / 8),
            under=self.label_2,
            show_dimensions=True,
            style="background: LightGreen"
        )

        self.label_4 = Label(
            self.auto_run,
            width=self.auto_run_back.width(),
            height=int(self.auto_run_back.height() / 8),
            under=self.label_3,
            show_dimensions=True,
            style="background: SkyBlue"
        )

        self.label_5 = Label(
            self.auto_run,
            width=self.auto_run_back.width(),
            height=int(self.auto_run_back.height() / 8),
            under=self.label_4,
            show_dimensions=True,
            style="background: LightGreen"
        )

        self.label_6 = Label(
            self.auto_run,
            width=self.auto_run_back.width(),
            height=int(self.auto_run_back.height() / 8),
            under=self.label_5,
            show_dimensions=True,
            style="background: SkyBlue"
        )

        self.label_7 = Label(
            self.auto_run,
            width=self.auto_run_back.width(),
            height=int(self.auto_run_back.height() / 8),
            under=self.label_6,
            show_dimensions=True,
            style="background: LightGreen"
        )

        self.label_8 = Label(
            self.auto_run,
            width=self.auto_run_back.width(),
            height=int(self.auto_run_back.height() / 8),
            under=self.label_7,
            show_dimensions=True,
            style="background: Orange"
        )

        '''self.label_1 = Label(
            self.auto_run,
            width=int(self.auto_run_back.width() / 2),
            height=int(self.auto_run_back.height() / 4),
            show_dimensions=True,
            style="background: LightGreen"
        )

        self.label_2 = Label(
            self.auto_run,
            width=int(self.auto_run_back.width() / 2),
            height=int(self.auto_run_back.height() / 4),
            right_from=self.label_1,
            show_dimensions=True,
            style="background: SkyBlue"
        )

        self.label_3 = Label(
            self.auto_run,
            width=int(self.auto_run_back.width() / 2),
            height=int(self.auto_run_back.height() / 4),
            under=self.label_1,
            show_dimensions=True,
            style="background: SkyBlue"
        )

        self.label_4 = Label(
            self.auto_run,
            width=int(self.auto_run_back.width() / 2),
            height=int(self.auto_run_back.height() / 4),
            right_from=self.label_3,
            show_dimensions=True,
            style="background: LightGreen"
        )

        self.label_5 = Label(
            self.auto_run,
            width=int(self.auto_run_back.width() / 2),
            height=int(self.auto_run_back.height() / 4),
            under=self.label_3,
            show_dimensions=True,
            style="background: LightGreen"
        )

        self.label_6 = Label(
            self.auto_run,
            width=int(self.auto_run_back.width() / 2),
            height=int(self.auto_run_back.height() / 4),
            right_from=self.label_5,
            show_dimensions=True,
            style="background: SkyBlue"
        )

        self.label_7 = Label(
            self.auto_run,
            width=int(self.auto_run_back.width() / 2),
            height=int(self.auto_run_back.height() / 4),
            under=self.label_5,
            show_dimensions=True,
            style="background: SkyBlue"
        )'''


    def set_window(self):
        self.setWindowTitle("Firestarter")
        self.setWindowIcon(QIcon("Files/Icons/favicon.ico"))
        self.setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT)
        self.show()

    # endregion

    # region GETTING TEST AGENTS

    def get_my_agents(self):
        self.agents_with_my_reaction = self.get_agents_with_my_reaction()
        agents_with_frontend_names = self.get_agents_with_frontend_names()

        return agents_with_frontend_names

    def get_agents_with_my_reaction(self):
        result = []

        for message in self.inventory_messages:
            if "reactions" in message:
                for _, value in message["reactions"].items():
                    if self.login in value["usernames"]:
                        result.append(message["msg"])

        return result

    def get_agents_with_frontend_names(self):
        result = {}

        for agent in self.agents_with_my_reaction:
            if "AltE" in agent:
                result[OSNameFrontend.AltE] = agent
            elif "AltW" in agent and "-K-" in agent:
                result[OSNameFrontend.AltWK] = agent
            elif "AltW" in agent:
                result[OSNameFrontend.AltW] = agent
            elif "Astra" in agent:
                result[OSNameFrontend.Astra] = agent
            elif "Windows-7" in agent or "Windows-8" in agent:
                result[OSNameFrontend.Windows78] = agent
            elif "Windows-10" in agent or "Windows-11" in agent:
                result[OSNameFrontend.Windows1011] = agent
            elif "dotnet" in agent:
                result[OSNameFrontend.WindowsNET] = agent
            else:
                raise Exception(f"Unknown test agent!\n{agent = }")

        return result

    def get_agents_all(self):
        result = []

        for message in self.inventory_messages:
            raw_name = message["msg"]

            if "Appium" in raw_name:
                continue

            if "AltE" in raw_name:
                name = OSNameFrontend.AltE
            elif "AltW" in raw_name and "-K-" in raw_name:
                name = OSNameFrontend.AltWK
            elif "AltW" in raw_name:
                name = OSNameFrontend.AltW
            elif "Astra" in raw_name:
                name = OSNameFrontend.Astra
            elif "Windows-7" in raw_name or "Windows-8" in raw_name:
                name = OSNameFrontend.Windows78
            elif "Windows-10" in raw_name or "Windows-11" in raw_name:
                name = OSNameFrontend.Windows1011
            elif "dotnet" in raw_name:
                name = OSNameFrontend.WindowsNET
            else:
                continue

            try:
                names = [e["names"][0] for e in message["reactions"].values()]
                logins = [e["usernames"][0] for e in message["reactions"].values()]

            except KeyError:
                names = None
                logins = None

            result.append(TestAgentDto(raw_name, name, names, logins))

        result = sorted(result, key=lambda x: x.agent)

        return result

    # endregion

    # region WORKERS

    def clear_min_os_inputs(self):
        self.clear_min_os_inputs_worker = ClearInputsWorker(self)
        self.clear_min_os_inputs_worker.start()
        self.clear_min_os_inputs_worker.wait()

    def set_markdown_button(self):
        self.set_markdown_button_worker = SetMarkdownButtonWorker(self)
        self.set_markdown_button_worker.start()
        self.set_markdown_button_worker.wait()

    def copy_markdown(self):
        self.copy_markdown_worker = CopyMarkdownWorker(self)
        self.copy_markdown_worker.start()
        self.copy_markdown_worker.wait()

    # endregion


if __name__ == "__main__":
    application = QApplication([])
    application.setStyle(QStyleFactory.create("Fusion"))
    window = MainWindow()
    application.exec_()
